const path = require('path')

function resolve (dir) {
  return path.join(__dirname, dir)
}

module.exports = {
  // publicPath: '/memnow_dev/',
  chainWebpack: (config) => {
    // TODO move to register

    config
      .resolve
      .alias
      .set('~', resolve('src'))
      .set('components', resolve('src/components'))
      .set('modules', resolve('src/modules'))
      .set('views', resolve('src/views'))
      .set('images', resolve('src/assets/images/index.js'))
  }
}
