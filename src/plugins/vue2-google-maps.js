import Vue from 'vue'
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyCawoTAJIFudT9Jjlvc6YH6CiXOZU-hfbU',
    libraries: 'places'
  }
})
