import { get } from 'lodash'

export function normalizeTokens (tokens) {
  return {
    accessToken: tokens.access_token,
    refreshToken: tokens.refresh_token
  }
}

export function normalizeUserProfile (profile) {
  return {
    name: profile.name,
    role: profile.role,
    id: profile.id
  }
}

export function normalizeUserPhotos (photo) {
  return {
    id: photo.id,
    photo: photo.photo,
    isConfirmed: photo.is_confirmed,
    created: photo.created_at,
    updated: photo.updated_at,
    rating: photo.nsfw_rating,
    nsfwClass: photo.nsfw_class

  }
}

export function normalizeUsers (user) {
  const avatar = get(user, 'avatar.large', '')
  return {
    id: user.id,
    avatar: avatar,
    birthday: user.birthday,
    name: user.name,
    username: user.username,
    isSystem: user.is_system,
    openId: user.open_id,
    photosBlockedFrom: user.photos_blocked_from,
    isBlocked: user.is_blocked,
    role: user.role,
    created: user.created_at,
    reqDate: user.last_req_date,
    followers: user.followers_count,
    followings: user.followings_count
  }
}

export function normalizeEvents (event) {
  const photoMain = get(event, 'photos[0].photo.large', '')
  const photoCover = get(event, 'cover.large', '')
  const caption = get(event, 'photos[0].caption', '')
  const lat = get(event, 'photos[0].lat', null)
  const long = get(event, 'photos[0].long', null)

  return {
    id: event.id,
    caption: caption,
    photoCover: photoCover,
    description: event.description,
    name: event.event_name,
    time: event.event_time,
    user: event.user,
    lat: lat,
    long: long,
    userId: event.user.id,
    photoMain: photoMain
  }
}

export function normalizeCampaign (campaign) {
  return {
    id: campaign.id,
    title: campaign.title,
    createdAt: campaign.created_at,
    status: campaign.status,
    content: campaign.content,
    count: campaign.users_count
  }
}

export function normalizeUploadPhoto (file) {
  return {
    filename: file.filename,
    url: file.url
  }
}

export function normalizeCampaignUsers (user) {
  const avatar = get(user, 'avatar.large', '')
  return {
    id: user.id,
    avatar: avatar,
    birthday: user.birthday,
    name: user.name,
    username: user.username,
    isSystem: user.is_system,
    openId: user.open_id,
    photosBlockedFrom: user.photos_blocked_from,
    isBlocked: user.is_blocked,
    role: user.role,
    created: user.created_at,
    reqDate: user.last_req_date,
    followers: user.followers_count,
    followings: user.followings_count
  }
}
