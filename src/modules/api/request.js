import axios from 'axios'
import endpoints from './endpoints'
import store from '@/store'
import router from '@/router'
import { stringify } from 'query-string'
import { isNull, isUndefined, pickBy } from 'lodash'

const request = axios.create({
  baseURL: endpoints.baseURL,
  paramsSerializer (params) {
    return stringify(pickBy(params, (value) => !isNull(value) && !isUndefined(value) && value !== ''))
  }
})

// interceptors request
request.interceptors.request.use(config => {
  const token = store.getters['auth/accessToken']

  if (token) {
    config.headers.Authorization = `Bearer ${token}`
  }

  return config
}, err => {
  return Promise.reject(err)
})

// interceptors response
request.interceptors.response.use((response) => {
  return response
}, async function refreshStrategy (error) {
  const { response } = error

  if (response &&
    response.status === 401 &&
    !response.config.__isRequestRetry &&
    store.getters['auth/isLoggedIn']
  ) {
    try {
      const accessToken = await store.dispatch(
        'auth/refreshAccessToken'
      )

      response.config.headers['Authorization'] = `Bearer ${accessToken}`

      response.config.__isRequestRetry = true

      return request(response.config)
    } catch (error) {
      store.dispatch('auth/deAuthenticate')
      router.replace({
        name: 'login'
      })
    }
  }

  return Promise.reject(error)
})

export default request
