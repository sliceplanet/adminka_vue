const endpoints = {
  baseURL: process.env.API_URL || 'https://admin-dev.memnow.ru/api/v1',

  login () {
    return `/users/login`
  },

  logout () {
    return `/users/logout`
  },

  refresh () {
    return `/users/refresh`
  },

  userProfile () {
    return `/users/my`
  },

  photos () {
    return `/photos`
  },

  moderate (eventId) {
    const pathParams = eventId ? `/${eventId}` : ''
    return `/photos/${pathParams}/moderate`
  },

  userBlock (eventId) {
    const pathParams = eventId ? `/${eventId}` : ''
    return `/users/${pathParams}/block`
  },

  users () {
    return `/users`
  },

  usersEdit (userId) {
    const pathParams = userId ? `/${userId}` : ''
    return `/users/${pathParams}`
  },

  userUploadPhoto () {
    return `/users/upload`
  },

  events () {
    return `/events`
  },

  eventsEdit (eventId) {
    const pathParams = eventId ? `/${eventId}` : ''
    return `/events/${pathParams}`
  },

  eventUploadPhoto () {
    return `/events/upload`
  },

  campaigns () {
    return `/campaigns`
  },

  campaignsUsers () {
    return `/campaigns/users`
  }
}

export default endpoints
