import api from '@/modules/api'

const types = {
  SET_ACCOUNTS: 'SET_ACCOUNTS',
  UPDATE_ACCOUNT: 'UPDATE_ACCOUNT',
  ADD_ACCOUNTS: 'ADD_ACCOUNTS',
  ADD_ACCOUNT: 'ADD_ACCOUNT',
  ACCOUNTS_CLEAR: 'ACCOUNTS_CLEAR'
}

const state = () => ({
  accounts: [],
  accountsLoaded: false,
  limitReached: false,

  defaultPagination: {},
  pagination: {}
})

const getters = {
  accounts (state) {
    return state.accounts
  },

  usersLimitReached (state) {
    return state.limitReached
  },

  accountsLoaded (state) {
    return state.accountsLoaded
  }
}

const actions = {
  async fetchAccounts ({ commit }, filters) {
    try {
      commit(types.ACCOUNTS_CLEAR)
      const accounts = await api.getAccountsSystem(filters)
      commit(types.SET_ACCOUNTS, accounts)
    } catch (error) {
      throw error
    }
  },

  async searchAccounts (store, filters) {
    try {
      const accounts = await api.getAccountsSystem(filters)
      return accounts
    } catch (error) {
      throw error
    }
  },

  async loadMoreAccounts ({ commit }, filters) {
    try {
      const accounts = await api.getAccountsSystem(filters)
      commit(types.ADD_ACCOUNTS, accounts)
    } catch (error) {
      throw error
    }
  },

  async addAccount ({ commit }, { name, birthday, avatar, userId }) {
    try {
      const account = await api.addAccount({
        name,
        birthday,
        avatar,
        userId
      })
      commit(types.ADD_ACCOUNT, account)
    } catch (error) {
      throw error
    }
  },
  async updateAccount ({ commit }, { name, birthday, avatar, id }) {
    try {
      const account = await api.updateAccount({
        name,
        birthday,
        avatar,
        id
      })
      commit(types.UPDATE_ACCOUNT, account)
    } catch (error) {
      throw error
    }
  }
}

const mutations = {
  [types.SET_ACCOUNTS] (state, accounts) {
    state.accounts = [...accounts]
    state.accountsLoaded = true
  },

  [types.ADD_ACCOUNTS] (state, accounts) {
    state.accounts = [...state.accounts, ...accounts]
    state.limitReached = accounts.length === 0
  },

  [types.ADD_ACCOUNT] (state, account) {
    state.accounts = [account, ...state.accounts]
  },

  [types.ACCOUNTS_CLEAR] (state) {
    state.accounts = []
    state.accountsLoaded = false
  },

  [types.UPDATE_ACCOUNT] (state, { id, name, birthday, avatar }) {
    state.accounts = state.accounts.map((account) => {
      if (account.id === id) {
        return {
          ...account,
          name,
          birthday,
          avatar

        }
      }
      return account
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
