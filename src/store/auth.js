import api from '@/modules/api'

const accessToken = localStorage.getItem('accessToken')
const refreshToken = localStorage.getItem('refreshToken')

const types = {
  SET_TOKENS: 'SET_TOKENS',
  CLEAR_TOKENS: 'CLEAR_TOKENS'
}

const state = () => ({
  accessToken,
  refreshToken
})

const getters = {
  isLoggedIn (state) {
    return Boolean(state.accessToken)
  },
  accessToken (state) {
    return state.accessToken
  },
  refreshToken (state) {
    return state.refreshToken
  }
}

const actions = {
  async authenticate ({ commit }, { username, password }) {
    try {
      const tokens = await api.login({
        username,
        password
      })

      commit(types.SET_TOKENS, tokens)

      localStorage.setItem('accessToken', tokens.accessToken)
      localStorage.setItem('refreshToken', tokens.refreshToken)
    } catch (error) {
      throw error
    }
  },

  async refreshAccessToken ({ getters, commit }) {
    const refreshToken = getters.refreshToken

    try {
      commit(types.SET_TOKENS, {
        accessToken: null,
        refreshToken
      })

      const { accessToken } = await api.refreshAccessToken(refreshToken)

      commit(types.SET_TOKENS, {
        accessToken,
        refreshToken
      })

      localStorage.setItem('accessToken', accessToken)

      return accessToken
    } catch (error) {
      throw error
    }
  },
  async deAuthenticate ({ commit }) {
    // Call API to revoke refresh token
    try {
      await api.logout()

      commit(types.CLEAR_TOKENS)

      localStorage.removeItem('accessToken')
      localStorage.removeItem('refreshToken')
    } catch (error) {
      throw error
    }
  }
}

const mutations = {
  [types.SET_TOKENS] (state, { accessToken, refreshToken }) {
    state.accessToken = accessToken
    state.refreshToken = refreshToken
  },
  [types.CLEAR_TOKENS] (state) {
    state.accessToken = ''
    state.refreshToken = ''
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
