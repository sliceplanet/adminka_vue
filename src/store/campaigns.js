import api from '@/modules/api'

const types = {
  USERS_SET: 'USERS_SET',
  USERS_ADD: 'USERS_ADD',
  USERS_CLEAR: 'USERS_CLEAR',
  UPDATE_USER: 'UPDATE_USER',
  CAMPAIGNS_SET: 'CAMPAIGNS_SET',
  CAMPAIGNS_ADD: 'CAMPAIGNS_ADD',
  CAMPAIGN_ADD: 'CAMPAIGN_ADD',
  CAMPAIGNS_CLEAR: 'CAMPAIGNS_CLEAR'
}

const state = () => ({
  users: [],
  usersLoaded: false,
  limitReached: false,

  campaigns: [],
  campaignsLoaded: false,
  campaignsLimitReached: false,
  count: null
})

const getters = {
  usersList (state) {
    return state.users
  },

  usersLoaded (state) {
    return state.usersLoaded
  },

  usersLimitReached (state) {
    return state.limitReached
  },

  list (state) {
    return state.campaigns
  },

  campaignsLoaded (state) {
    return state.campaignsLoaded
  },

  campaignsLimitReached (state) {
    return state.campaignsLimitReached
  },

  usersCount (state) {
    return state.count
  }
}

const actions = {
  async fetchUsers ({ commit }, filters) {
    try {
      commit(types.USERS_CLEAR)
      const { users, totalCount } = await api.getCampaignUsers(filters)
      commit(types.USERS_SET, { users, totalCount })
    } catch (error) {
      throw error
    }
  },

  async getUsers ({ commit }, filters) {
    try {
      const { users } = await api.getCampaignUsers(filters)
      commit(types.USERS_ADD, users)
    } catch (error) {
      throw error
    }
  },

  async updateUserStatus ({ commit }, { isBlocked, userId }) {
    try {
      await api.updateUserBlockStatus({
        isBlocked,
        userId
      })
      commit(types.UPDATE_USER, { isBlocked, userId })
    } catch (error) {
      throw error
    }
  },

  async createCampaign ({ commit }, payload) {
    try {
      const campaign = await api.campaignCreate(payload)

      commit(types.CAMPAIGN_ADD, campaign)
    } catch (error) {
      throw error
    }
  },

  async fetchCampaigns ({ commit }, filters) {
    try {
      commit(types.CAMPAIGNS_CLEAR)

      const campaigns = await api.getCampaigns(filters)

      commit(types.CAMPAIGNS_SET, campaigns)
    } catch (error) {
      throw error
    }
  },

  async loadCampaigns ({ commit }, filters) {
    try {
      const campaigns = await api.getCampaigns(filters)

      commit(types.CAMPAIGNS_ADD, campaigns)
    } catch (error) {
      throw error
    }
  }
}

const mutations = {
  [types.USERS_SET] (state, { users, totalCount }) {
    state.users = [...users]
    state.count = totalCount
    state.usersLoaded = true
  },
  [types.USERS_ADD] (state, users) {
    state.users = [...state.users, ...users]
    state.limitReached = users.length === 0
  },
  [types.USERS_CLEAR] (state) {
    state.users = []
    state.usersLoaded = false
  },
  [types.UPDATE_USER] (state, { userId, isBlocked }) {
    state.users = state.users.map((user) => {
      if (user.id === userId) {
        return {
          ...user,
          isBlocked
        }
      }
      return user
    })
  },
  [types.CAMPAIGNS_SET] (state, campaigns) {
    state.campaigns = [...campaigns]
    state.campaignsLoaded = true
  },
  [types.CAMPAIGNS_ADD] (state, campaigns) {
    state.campaigns = [...state.campaigns, ...campaigns]
    state.campaignsLimitReached = campaigns.length === 0
  },
  [types.CAMPAIGNS_CLEAR] (state) {
    state.campaigns = []
    state.campaignsLoaded = false
  },
  [types.CAMPAIGN_ADD] (state, campaign) {
    state.campaigns = [campaign, ...state.campaigns]
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
