import api from '@/modules/api'

const types = {
  SET_PHOTOS: 'SET_PHOTOS',
  UPDATE_PHOTO: 'UPDATE_PHOTO',
  ADD_PHOTOS: 'ADD_PHOTOS'
}

const state = () => ({
  photos: [],
  photosLoaded: false,
  limitReached: false,
  defaultPagination: {},
  pagination: {}
})

const getters = {
  photos (state) {
    return state.photos
  },

  photosLoaded (state) {
    return state.photosLoaded
  }
}

const actions = {
  async fetchPhotos ({ commit }, filters) {
    try {
      const photos = await api.getPhotos(filters)

      commit(types.SET_PHOTOS, photos)
    } catch (error) {
      throw error
    }
  },
  async loadMorePhotos ({ commit }, filters) {
    try {
      const photos = await api.getPhotos(filters)

      commit(types.ADD_PHOTOS, photos)
    } catch (error) {
      throw error
    }
  },
  async moderatePhoto ({ commit }, { isConfirmed, photoId, nsfwClass }) {
    try {
      await api.moderatePhoto({
        isConfirmed,
        photoId,
        nsfwClass
      })
      commit(types.UPDATE_PHOTO, photoId)
    } catch (error) {
      throw error
    }
  }
}

const mutations = {
  [types.SET_PHOTOS] (state, photos) {
    state.photos = [...photos]
    state.photosLoaded = true
  },

  [types.ADD_PHOTOS] (state, photos) {
    state.photos = [...state.photos, ...photos]
  },

  [types.UPDATE_PHOTO] (state, photoId) {
    state.photos = state.photos.filter(photo => photo.id !== photoId)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
