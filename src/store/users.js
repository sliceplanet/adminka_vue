import api from '@/modules/api'

const types = {
  SET_USERS: 'SET_USERS',
  ADD_USER: 'ADD_USER',
  USERS_CLEAR: 'USERS_CLEAR',
  UPDATE_USER: 'UPDATE_USER'
}

const state = () => ({
  users: [],
  usersLoaded: false,
  limitReached: false,
  defaultPagination: {},
  pagination: {}
})

const getters = {
  users (state) {
    return state.users
  },

  usersLoaded (state) {
    return state.usersLoaded
  },

  limitReached (state) {
    return state.limitReached
  }
}

const actions = {
  async fetchUsers ({ commit }, filters) {
    try {
      commit(types.USERS_CLEAR)

      const users = await api.getUsers(filters)

      commit(types.SET_USERS, users)
    } catch (error) {
      throw error
    }
  },
  async loadMoreUsers ({ commit }, filters) {
    try {
      const users = await api.getUsers(filters)

      commit(types.ADD_USER, users)
    } catch (error) {
      throw error
    }
  },
  async updateUserStatus ({ commit }, { isBlocked, userId }) {
    try {
      await api.updateUserBlockStatus({
        isBlocked,
        userId
      })
      commit(types.UPDATE_USER, { isBlocked, userId })
    } catch (error) {
      throw error
    }
  },
  async uploadPhoto (store, { file }) {
    try {
      const fileUploaded = await api.userUploadPhoto({ file })
      return fileUploaded
    } catch (error) {
      throw error
    }
  }

}

const mutations = {
  [types.SET_USERS] (state, users) {
    state.users = [...users]
    state.usersLoaded = true
  },

  [types.ADD_USER] (state, users) {
    state.users = [...state.users, ...users]
    state.limitReached = users.length === 0
  },

  [types.USERS_CLEAR] (state) {
    state.users = []
    state.usersLoaded = false
  },

  [types.UPDATE_USER] (state, { userId, isBlocked }) {
    state.users = state.users.map((user) => {
      if (user.id === userId) {
        return {
          ...user,
          isBlocked
        }
      }
      return user
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
