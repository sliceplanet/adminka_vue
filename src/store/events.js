import api from '@/modules/api'

const types = {
  SET_EVENTS: 'SET_EVENTS',
  UPDATE_EVENT: 'UPDATE_EVENT',
  ADD_EVENTS: 'ADD_EVENTS',
  ADD_EVENT: 'ADD_EVENT',
  EVENTS_CLEAR: 'EVENTS_CLEAR'
}

const state = () => ({
  events: [],
  eventsLoaded: false,
  limitReached: false,
  defaultPagination: {},
  pagination: {}
})

const getters = {
  events (state) {
    return state.events
  },

  eventsLimitReached (state) {
    return state.limitReached
  },

  eventsLoaded (state) {
    return state.eventsLoaded
  }
}

const actions = {
  async fetchEvents ({ commit }, filters) {
    try {
      commit(types.EVENTS_CLEAR)
      const events = await api.getEvents(filters)
      commit(types.SET_EVENTS, events)
    } catch (error) {
      throw error
    }
  },
  async loadMoreEvents ({ commit }, filters) {
    try {
      const events = await api.getEvents(filters)
      commit(types.ADD_EVENTS, events)
    } catch (error) {
      throw error
    }
  },
  async updateEvent ({ commit }, { name, time, description, photoMain, userId, caption, photoCover, id, lat, long }) {
    try {
      const event = await api.eventsEdit({
        name,
        time,
        description,
        photoMain,
        userId,
        caption,
        photoCover,
        id,
        lat,
        long
      })
      commit(types.UPDATE_EVENT, event)
    } catch (error) {
      throw error
    }
  },

  async addEvent ({ commit }, { name, time, description, photoMain, userId, caption, photoCover, lat, long }) {
    try {
      const event = await api.addEvent({
        name,
        time,
        description,
        photoMain,
        userId,
        caption,
        photoCover,
        lat,
        long
      })
      commit(types.ADD_EVENT, event)
    } catch (error) {
      throw error
    }
  },

  async uploadPhoto (store, { file }) {
    try {
      const fileUploaded = await api.eventUploadPhoto({ file })
      return fileUploaded
    } catch (error) {
      throw error
    }
  }
}

const mutations = {
  [types.SET_EVENTS] (state, events) {
    state.events = [...events]
    state.eventsLoaded = true
  },

  [types.ADD_EVENTS] (state, events) {
    state.events = [...state.events, ...events]
    state.limitReached = events.length === 0
  },

  [types.EVENTS_CLEAR] (state) {
    state.events = []
    state.eventsLoaded = false
  },

  [types.ADD_EVENT] (state, event) {
    state.events = [event, ...state.events]
  },

  [types.UPDATE_EVENT] (state, { name, time, description, photoMain, userId, caption, photoCover, id, lat, long }) {
    state.events = state.events.map((event) => {
      if (event.id === id) {
        return {
          ...event,
          name,
          time,
          description,
          photoMain,
          userId,
          caption,
          photoCover,
          lat,
          long
        }
      }
      return event
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
