import Vue from 'vue'
import Vuex from 'vuex'
import auth from './auth'
import user from './user'
import photos from './photos'
import users from './users'
import accounts from './accounts'
import events from './events'
import campaigns from './campaigns'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,
    user,
    photos,
    users,
    accounts,
    events,
    campaigns
  }
})
