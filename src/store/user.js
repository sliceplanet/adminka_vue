import api from '@/modules/api'

const types = {
  SET_PROFILE: 'SET_PROFILE',
  CLEAR_PROFILE: 'CLEAR_PROFILE'
}

const state = () => ({
  profileLoaded: false,
  profile: ''
})

const getters = {
  profile (state) {
    return state.profile
  }
}

const actions = {
  async getProfile ({ commit }) {
    try {
      const profile = await api.getUserProfile()

      commit(types.SET_PROFILE, profile)
    } catch (error) {
      throw error
    }
  },
  clearProfile ({ commit }) {
    commit(types.CLEAR_PROFILE)
  }

}

const mutations = {
  [types.SET_PROFILE] (state, profile) {
    state.profileLoaded = true
    state.profile = { ...profile }
  },
  [types.CLEAR_PROFILE] (state) {
    state.profileLoaded = false
    state.profile = ''
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
