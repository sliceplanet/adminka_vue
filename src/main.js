import Vue from 'vue'

import './plugins'
import './mixins'
import eventBus from './plugins/event-bus'

import './filters'
import router from './router'
import store from './store'

import App from './App.vue'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  data: {
    eventBus
  },
  render: h => h(App)
}).$mount('#app')
