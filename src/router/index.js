import Vue from 'vue'
import Router from 'vue-router'
import store from '@/store'

import Photos from 'views/Photos'
import Management from 'views/Management'
import Accounts from 'views/Accounts'
import Users from 'views/Accounts/Users'
import Events from 'views/Accounts/Events'

const Login = () => import(/* webpackChunkName: "login" */ 'views/Login')
const Campaigns = () => import(/* webpackChunkName: "Campaigns" */ 'views/Campaigns')
const CampaignsList = () => import(/* webpackChunkName: "Campaigns" */ 'views/Campaigns/CampaignsList')
const CampaignsStats = () => import(/* webpackChunkName: "Campaigns" */ 'views/Campaigns/CampaignsStats')

Vue.use(Router)

const router = new Router({
  // mode: 'history',
  history: 'hash',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/',
      name: 'home',
      component: Photos,
      meta: {
        auth: true,
        title: 'Модерация фото'
      }
    },
    {
      path: '/management',
      name: 'management',
      component: Management,
      meta: {
        auth: true,
        title: 'Управление пользователями'
      }
    },
    {
      path: '/accounts',
      component: Accounts,
      children: [{
        path: '',
        name: 'accounts-events',
        component: Events,
        meta: {
          auth: true,
          title: 'Системные аккаунты'
        }
      }, {
        path: 'users',
        component: Users,
        name: 'accounts-users',
        meta: {
          auth: true,
          title: 'Системные аккаунты'
        }
      }],
      meta: {
        auth: true,
        title: 'Системные аккаунты'
      }
    },
    {
      path: '/campaigns',
      component: Campaigns,
      meta: {
        auth: true,
        title: 'Рассылка push-сообщений'
      },
      children: [
        {
          path: '',
          name: 'campaigns',
          meta: {
            auth: true,
            title: 'Рассылка push-сообщений'
          },
          component: CampaignsList
        },
        {
          path: 'stats',
          name: 'campaigns-stats',
          meta: {
            auth: true,
            title: 'Рассылка push-сообщений'
          },
          component: CampaignsStats
        }
      ]
    }
  ]
})

router.beforeEach((to, from, next) => {
  const isLoggedIn = store.getters['auth/isLoggedIn']
  if (!isLoggedIn && to.meta.auth) {
    return next({ name: 'login' })
  } else if (isLoggedIn && !to.meta.auth) {
    return next({ name: 'home' })
  }

  next()
})
export default router
